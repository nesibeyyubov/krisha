from django.contrib import admin

# Register your models here.
from .models import *

admin.site.register([
		MainAbout,MiddleSection,ContactInfo,
		Testimonial,AboutMain,
		Slider,AboutFourStep,OurTeam,EventBackImage,
		Event,Speaker,LogoOfSponsor,Sponsors])