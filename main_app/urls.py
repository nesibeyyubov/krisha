from django.urls import path,include

from .views import *

urlpatterns = [
    path('', home,name='home'),
    path('contact/', contact,name='contact'),
    path('about/', about,name='about'),
    path('team/', team,name='team'),
    path('events/', event,name='event'), 
    path('sponsors/', sponsors,name='sponsors'),
    path('donations/', donations,name='donations'),


]
