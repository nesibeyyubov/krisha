from django.shortcuts import render
from django.contrib import messages
from django.core.mail import send_mail
from .models import *
import stripe
stripe.api_key = "sk_test_51HAz1IGmvxqR6k5aHUCEPixQ7FiLgogfcKL045Sp0q6Ohib9IeO0pLjswAjDzgSR5sQai3cdFQLvxH2cDWCgrRY200uhr88cIO"

# Create your views here.


def home(request):
	main_about=MainAbout.objects.all().first()
	testomonials=Testimonial.objects.all()
	sliders=Slider.objects.all()

	return render(request,'index.html',{"main_about":main_about,'testomonials':testomonials,'sliders':sliders})

def contact(request):
	contact=ContactInfo.objects.all().first()
	if request.method=='POST':
		send_mail(
			request.POST.get('name')+" "+request.POST.get('phone'),
			request.POST.get('message')+' \nfrom {}'.format(request.POST.get('email')),
			request.POST.get('email'),
			['elvin.ceferov.bmu@gmail.com'],
			fail_silently=False,
		)
		messages.success(request, 'Mesajınız uğurla göndərildi')
	return render(request,'contact.html',{"contact":contact})


def about(request):
	obj=MiddleSection.objects.all()
	left=obj.first()
	right=obj[1]
	main=AboutMain.objects.all().first()
	abtsteps=AboutFourStep.objects.all()[:4]
	return render(request,'about.html',{'abtsteps':abtsteps,"main":main,'left':left,'right':right})


	
def event(request):
	image=EventBackImage.objects.all().first()
	event=Event.objects.all().first()
	speakers=Speaker.objects.all()
	return render(request,'events.html',{"image":image,"event":event,'speakers':speakers})
 

def team(request):
	members=OurTeam.objects.all()
	return render(request,'team.html',{'members':members})


def sponsors(request):
	logos=LogoOfSponsor.objects.all()
	sponsors=Sponsors.objects.all()
	return render(request,'sponsors.html',{"logos":logos,"sponsors":sponsors})




def donations(request):
	logos=LogoOfSponsor.objects.all()
	if request.method=='POST':
		stripe.Charge.create(
		amount=int(request.POST.get("amount"))*100,
		currency="eur",
		source="tok_visa",
		description="My First Test Charge (created for API docs)",
		)
	
	return render(request,'donations.html',{"logos":logos})
