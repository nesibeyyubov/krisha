from django.db import models

# Create your models here.


class Slider(models.Model):
	image=models.ImageField(upload_to='slider')
	text=models.TextField(blank=True, null=True)

	def __str__(self):
		return self.text[:4]
	


class MainAbout(models.Model):
	title=models.CharField(max_length=255)	
	description=models.TextField()
		
	
	class Meta:
		verbose_name = ("MainAbout")
		verbose_name_plural = ("MainAbout")

	def __str__(self):
		return "bu Hisse yalniz bir eded olmalilidr"
	
class ContactInfo(models.Model):
	email=models.CharField(max_length=150)
	address=models.CharField(max_length=150)
	number=models.CharField(max_length=150)


	class Meta:
		verbose_name = ("ContactInfo")
		verbose_name_plural = ("ContactInfo")

	def __str__(self):
		return 'Bu hisse yalniz bir eded olmaliidir'


	
class Testimonial(models.Model):
	quote=models.TextField()
	quote_by=models.CharField(max_length=150)


	class Meta:
		verbose_name = ("Testimonial")
		verbose_name_plural = ("Testimonial")

	def __str__(self):
		return self.quote_by



class AboutFourStep(models.Model):
	title=models.CharField(max_length=150)
	text=models.TextField()

	class Meta:
		verbose_name = ("AboutFourStep")
		verbose_name_plural = ("AboutFourSteps")

	def __str__(self):
		return self.title


class MiddleSection(models.Model):
	image=models.ImageField()
	sub_title=models.CharField(max_length=50,blank=True, null=True)
	title=models.CharField(max_length=150)
	desc=models.TextField()


class AboutMain(models.Model):
	background_image=models.ImageField()
	title=models.CharField( max_length=150)
	desc=models.TextField(blank=True, null=True)


	class Meta:
		verbose_name = ("AboutMain")
		verbose_name_plural = ("AboutMain")

	def str(self):
		return 'Bu hisse bir eded olmalidir'



class OurTeam(models.Model):
	image=models.ImageField()
	full_name=models.CharField(max_length=50)
	title=models.CharField(max_length=150)
	desc=models.TextField(blank=True, null=True)

	def __str__(self):
		return self.full_name
	
class EventBackImage(models.Model):
	image=models.ImageField()
	def str(self):
		return 'Bu hisse bir eded olmalidir'



class Event(models.Model):
	event_name=models.CharField(max_length=50)
	preview_link=models.TextField(blank=True, null=True)
	desc=models.TextField(blank=True, null=True)
	event_day=models.CharField(max_length=50)
	event_month=models.CharField(max_length=50)
	def __str__(self):
		return self.event_name
	



class Speaker(models.Model):
	image=models.ImageField()
	full_name=models.CharField(max_length=50)
	title=models.CharField(max_length=150)
	desc=models.TextField(blank=True, null=True)

	def __str__(self):
		return self.full_name
	

class LogoOfSponsor(models.Model):
	image=models.ImageField()

class Sponsors(models.Model):
	image=models.ImageField()
	link=models.TextField()
	title=models.CharField(max_length=150)
	desc=models.TextField(blank=True, null=True)

	def __str__(self):
		return self.title