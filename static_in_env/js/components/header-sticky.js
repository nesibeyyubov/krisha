// Header Sticky
var HeaderSticky = function() {
  'use strict';

  // Handle Header Sticky
  var handleHeaderSticky = function() {
    // On loading, check to see if more than 15rem, then add the class
    if ($('.js__header-sticky').offset().top > 15) {
      $('.js__header-sticky').addClass('s-header__shrink');
    }

    // On scrolling, check to see if more than 15rem, then add the class
    $(window).on('scroll', function(e) {
      e.preventDefault();
      if ($('.js__header-sticky').offset().top > 15) {
        
        
        $('.js__header-sticky').addClass('s-header__shrink 15');
        $('#show').css('color','black');
        console.log(document.getElementById("show"));
        $('#show').addClass('salaaam');
      } else {
        $('.js__header-sticky').removeClass('s-header__shrink');
        $('#show').css('color','white');
      }
    });
  }

  return {
    init: function() {
      handleHeaderSticky(); // initial setup for Header Sticky
    }
  }
}();

$(document).ready(function() {
  HeaderSticky.init();
});